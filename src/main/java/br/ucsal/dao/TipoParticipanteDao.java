package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.TipoParticipante;

public interface TipoParticipanteDao {
	public void add(TipoParticipante tp);
	public void edit(TipoParticipante p);
	public void delete(int tpId);
	public TipoParticipante getTipoParticipante(int tpId);
	public List getAllTipoParticipante();
}
