package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.Participante;

public interface ParticipanteDao {
	public void add(Participante p);
	public void edit(Participante p);
	public void delete(int pId);
	public Participante getParticipante(int pId);
	public List getAllParticipante();
}
