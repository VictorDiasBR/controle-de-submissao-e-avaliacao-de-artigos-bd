package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.DadosCartao;

public interface DadosCartaoDao {

	public void add(DadosCartao d);
	public void edit(DadosCartao d);
	public void delete(int Id);
	public DadosCartao getDadosCartao  (int Id);
	public List getAllDadosCartao();
	
}
