package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.FeedBackRevisor;


public interface FeedBackRevisorDao {
	
	public void add(FeedBackRevisor f);
	public void edit(FeedBackRevisor f);
	public void delete(int Id);
	public FeedBackRevisor getFeedBackRevisor (int Id);
	public List getAllFeedBackRevisor();
}
