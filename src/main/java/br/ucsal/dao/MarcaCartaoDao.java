package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.MarcaCartao;

public interface MarcaCartaoDao {
	public void add(MarcaCartao m);
	public void edit(MarcaCartao m);
	public void delete(int Id);
	public MarcaCartao getMarcaCartao(int Id);
	public List getAllMarcaCartao();
}
