package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.Artigo;

public interface ArtigoDao {

	public void add(Artigo a);
	public void edit(Artigo a);
	public void delete(int Id);
	public Artigo getArtigo (int Id);
	public List getAllArtigo();
}
