package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.Congresso;

public interface CongressoDao {

	public void add(Congresso c);
	public void edit(Congresso c);
	public void delete(int Id);
	public Congresso getCongresso  (int Id);
	public List getAllCongresso();
}
