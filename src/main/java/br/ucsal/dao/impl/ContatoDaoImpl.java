package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import br.ucsal.dao.ContatoDao;
import br.ucsal.model.Contato;

public class ContatoDaoImpl implements ContatoDao{

	private SessionFactory session;
	@Override
	public void add(Contato c) {
		
		session.getCurrentSession().save(c);
	}

	@Override
	public void edit(Contato c) {
		
		session.getCurrentSession().update(c);	
	}

	@Override
	public void delete(int Id) {
		
		session.getCurrentSession().delete(Id);
	}

	@Override
	public Contato  getContato  (int Id) {
		// TODO Auto-generated method stub
		return (Contato) session.getCurrentSession().get(Contato.class,Id);
	}

	@Override
	public List getAllContato () {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_contato").list();
	}
}
