package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import br.ucsal.dao.DadosCartaoDao;
import br.ucsal.model.DadosCartao;


public class DadosCartaoDaoImpl implements DadosCartaoDao{

	private SessionFactory session;
	@Override
	public void add(DadosCartao d) {
		
		session.getCurrentSession().save(d);
	}

	@Override
	public void edit(DadosCartao d) {
		
		session.getCurrentSession().update(d);	
	}

	@Override
	public void delete(int Id) {
		
		session.getCurrentSession().delete(Id);
	}

	@Override
	public DadosCartao  getDadosCartao  (int Id) {
		// TODO Auto-generated method stub
		return (DadosCartao) session.getCurrentSession().get(DadosCartao.class,Id);
	}

	@Override
	public List getAllDadosCartao () {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_dados_cartao").list();
	}
}
