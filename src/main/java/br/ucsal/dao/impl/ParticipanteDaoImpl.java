package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import br.ucsal.dao.ParticipanteDao;
import br.ucsal.model.Participante;

@Repository
public class ParticipanteDaoImpl implements ParticipanteDao {
	
	private SessionFactory session;
	@Override
	public void add(Participante p) {
		
		session.getCurrentSession().save(p);
	}

	@Override
	public void edit(Participante p) {
		
		session.getCurrentSession().update(p);
		
	}

	@Override
	public void delete(int pId) {
		
		session.getCurrentSession().delete(pId);
	}

	@Override
	public Participante getParticipante(int pId) {
		// TODO Auto-generated method stub
		return (Participante) session.getCurrentSession().get(Participante.class,pId);
	}

	@Override
	public List getAllParticipante() {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_participante").list();
	}

}
