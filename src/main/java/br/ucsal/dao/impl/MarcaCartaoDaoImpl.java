package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import br.ucsal.dao.MarcaCartaoDao;
import br.ucsal.model.MarcaCartao;

public class MarcaCartaoDaoImpl implements MarcaCartaoDao{
	private SessionFactory session;
	@Override
	public void add(MarcaCartao m) {
		
		session.getCurrentSession().save(m);
	}

	@Override
	public void edit(MarcaCartao m) {
		
		session.getCurrentSession().update(m);	
	}

	@Override
	public void delete(int Id) {
		
		session.getCurrentSession().delete(Id);
	}

	@Override
	public MarcaCartao  getMarcaCartao (int Id) {
		// TODO Auto-generated method stub
		return (MarcaCartao) session.getCurrentSession().get(MarcaCartao.class,Id);
	}

	@Override
	public List getAllMarcaCartao () {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_marca_cartao").list();
	}
}
