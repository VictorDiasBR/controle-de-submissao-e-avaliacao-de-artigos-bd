package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import br.ucsal.dao.TipoParticipanteDao;
import br.ucsal.model.TipoParticipante;

public class TipoParticipanteDaoImpl implements TipoParticipanteDao {

	private SessionFactory session;
	@Override
	public void add(TipoParticipante tp) {
		
		session.getCurrentSession().save(tp);
	}

	@Override
	public void edit(TipoParticipante tp) {
		
		session.getCurrentSession().update(tp);
		
	}

	@Override
	public void delete(int tpId) {
		
		session.getCurrentSession().delete(tpId);
	}

	@Override
	public TipoParticipante getTipoParticipante(int tpId) {
		// TODO Auto-generated method stub
		return (TipoParticipante) session.getCurrentSession().get(TipoParticipante.class,tpId);
	}

	@Override
	public List getAllTipoParticipante() {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_tipo_participante").list();
	}

}
