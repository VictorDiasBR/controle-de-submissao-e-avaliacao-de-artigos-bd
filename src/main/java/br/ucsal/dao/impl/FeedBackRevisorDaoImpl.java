package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import br.ucsal.dao.FeedBackRevisorDao;
import br.ucsal.model.FeedBackRevisor;

public class FeedBackRevisorDaoImpl implements FeedBackRevisorDao {

	private SessionFactory session;
	private Session sf = session.openSession();
	Transaction tx = null;
	@Override
	public void add(FeedBackRevisor f) {
		try {
			tx = sf.beginTransaction();
			session.getCurrentSession().save(f);
			tx.commit();
			sf.close();
		}catch(Exception e) {
			if(tx!=null) tx.rollback();
			sf.close();
			e.printStackTrace();
		}
	}

	@Override
	public void edit(FeedBackRevisor f) {
		
		session.getCurrentSession().update(f);	
	}

	@Override
	public void delete(int Id) {
		
		session.getCurrentSession().delete(Id);
	}

	@Override
	public FeedBackRevisor   getFeedBackRevisor  (int Id) {
		// TODO Auto-generated method stub
		return (FeedBackRevisor) session.getCurrentSession().get(FeedBackRevisor.class,Id);
	}

	@Override
	public List getAllFeedBackRevisor () {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_feedback_revisor").list();
	}
}
