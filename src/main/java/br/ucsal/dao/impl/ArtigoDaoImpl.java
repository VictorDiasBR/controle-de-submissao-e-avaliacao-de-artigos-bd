package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import br.ucsal.dao.ArtigoDao;
import br.ucsal.model.Artigo;

public class ArtigoDaoImpl implements ArtigoDao{

	private SessionFactory session;
	@Override
	public void add(Artigo a) {
		
		session.getCurrentSession().save(a);
	}

	@Override
	public void edit(Artigo a) {
		
		session.getCurrentSession().update(a);	
	}

	@Override
	public void delete(int Id) {
		
		session.getCurrentSession().delete(Id);
	}

	@Override
	public Artigo  getArtigo  (int Id) {
		// TODO Auto-generated method stub
		return (Artigo) session.getCurrentSession().get(Artigo.class,Id);
	}

	@Override
	public List getAllArtigo () {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_artigo").list();
	}
}
