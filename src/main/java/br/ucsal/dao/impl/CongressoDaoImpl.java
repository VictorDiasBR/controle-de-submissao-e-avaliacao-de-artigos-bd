package br.ucsal.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import br.ucsal.dao.CongressoDao;
import br.ucsal.model.Congresso;

public class CongressoDaoImpl implements CongressoDao {
	
	private SessionFactory session;
	@Override
	public void add(Congresso c) {
		
		session.getCurrentSession().save(c);
	}

	@Override
	public void edit(Congresso c) {
		
		session.getCurrentSession().update(c);	
	}

	@Override
	public void delete(int Id) {
		
		session.getCurrentSession().delete(Id);
	}

	@Override
	public Congresso  getCongresso  (int Id) {
		// TODO Auto-generated method stub
		return (Congresso) session.getCurrentSession().get(Congresso.class,Id);
	}

	@Override
	public List getAllCongresso () {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from tab_congresso").list();
	}
}
