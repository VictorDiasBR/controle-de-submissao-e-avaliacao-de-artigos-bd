package br.ucsal.dao;

import java.util.List;

import br.ucsal.model.Contato;

public interface ContatoDao {

	public void add(Contato c);
	public void edit(Contato c);
	public void delete(int Id);
	public Contato getContato  (int Id);
	public List getAllContato();
}
