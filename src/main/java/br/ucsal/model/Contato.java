package br.ucsal.model;

import javax.persistence.*;

@Entity
@Table(name = "tab_contato", schema = "public")
public class Contato {
    @Id
	@GeneratedValue
	private Integer id;
    private String endereco;
	private String telefone;
	private String email;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
