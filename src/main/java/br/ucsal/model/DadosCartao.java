package br.ucsal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "tab_dados_cartao", schema = "public")
public class DadosCartao {

	@Id
	@GeneratedValue
	private Integer id;
	@Column(name="nome_titular",nullable= false)
	private String nome;
	@Column(name="numero_cartao",nullable= false)
	private String numero;
	@Temporal(TemporalType.DATE)
	@Column(name = "data_vencimento", nullable = false)
	private Date data;
	@JoinColumn(name = "id_bandeira_cartao", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name="fk_bandeira_cartao"))
	@ManyToOne
	private MarcaCartao bandeira;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
}
