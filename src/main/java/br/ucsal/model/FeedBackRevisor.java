package br.ucsal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tab_feedback_revisor", schema = "public")
public class FeedBackRevisor {

	@Id
	@GeneratedValue
	private Integer id;
	@ManyToOne
	private Participante participanteRevisor;
	@ManyToOne
	private Artigo artigo;
	private Double nota;
	private String comentario;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Participante getParticipanteRevisor() {
		return participanteRevisor;
	}
	public void setParticipanteRevisor(Participante participanteRevisor) {
		this.participanteRevisor = participanteRevisor;
	}
	public Artigo getArtigo() {
		return artigo;
	}
	public void setArtigo(Artigo artigo) {
		this.artigo = artigo;
	}
	public Double getNota() {
		return nota;
	}
	public void setNota(Double nota) {
		this.nota = nota;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	
	
}
