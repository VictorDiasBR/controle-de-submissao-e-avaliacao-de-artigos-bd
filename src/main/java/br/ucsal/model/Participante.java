package br.ucsal.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "tab_participante", schema = "public")
public class Participante {
	
	@Id
	@Column(name= "id_numero_inscricao")
	private Integer numInscricao;
	@ManyToOne
	private DadosCartao cartao;
	@ManyToOne
	private Contato contato;
	@ManyToOne
	private Congresso congresso;
	private String nome;
	@Column(name= "local_emprego")
	private String localEmprego;
	@ManyToOne
	private TipoParticipante tipo;
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="participante_id_numero_inscricao")
	private List <Artigo> artigos;
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="participanteRevisor_id_numero_inscricao")
	private List <FeedBackRevisor> feedBacks;
	
	public Integer getNumInscricao() {
		return numInscricao;
	}
	public void setNumInscricao(Integer numInscricao) {
		this.numInscricao = numInscricao;
	}
	public DadosCartao getCartao() {
		return cartao;
	}
	public void setCartao(DadosCartao cartao) {
		this.cartao = cartao;
	}
	public Contato getContato() {
		return contato;
	}
	public void setContato(Contato contato) {
		this.contato = contato;
	}
	public Congresso getCongresso() {
		return congresso;
	}
	public void setCongresso(Congresso congresso) {
		this.congresso = congresso;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLocalEmprego() {
		return localEmprego;
	}
	public void setLocalEmprego(String localEmprego) {
		this.localEmprego = localEmprego;
	}
	public TipoParticipante getTipo() {
		return tipo;
	}
	public void setTipo(TipoParticipante tipo) {
		this.tipo = tipo;
	}
	
	
}
