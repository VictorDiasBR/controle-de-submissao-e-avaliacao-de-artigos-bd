package br.ucsal.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tab_artigo", schema = "public")
public class Artigo {

	@Id
	@GeneratedValue
	private Integer id;
	@ManyToOne
	private Participante participante;
	@Column(name= "titulo_artigo")
	private String tituloArtigo;
	@Column(name= "nome_autor")
	private String nomeAutor;
	@Column(name= "email_autor")
	private String emailAutor;
	@Column(name= "resumo_artigo")
	private String resumoArtigo;
	@ManyToOne
	private Congresso congresso;
	@Column(name="link_artigo")
	private String linkArtigo;
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="artigo_id")
	private List <FeedBackRevisor> feedBacks;
	
	public String getLinkArtigo() {
		return linkArtigo;
	}
	public void setLinkArtigo(String linkArtigo) {
		this.linkArtigo = linkArtigo;
	}
	public Congresso getCongresso() {
		return congresso;
	}
	public void setCongresso(Congresso congresso) {
		this.congresso = congresso;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Participante getParticipante() {
		return participante;
	}
	public void setParticipante(Participante participante) {
		this.participante = participante;
	}
	public String getTituloArtigo() {
		return tituloArtigo;
	}
	public void setTituloArtigo(String tituloArtigo) {
		this.tituloArtigo = tituloArtigo;
	}
	public String getNomeAutor() {
		return nomeAutor;
	}
	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}
	public String getEmailAutor() {
		return emailAutor;
	}
	public void setEmailAutor(String emailAutor) {
		this.emailAutor = emailAutor;
	}
	public String getResumoArtigo() {
		return resumoArtigo;
	}
	public void setResumoArtigo(String resumoArtigo) {
		this.resumoArtigo = resumoArtigo;
	}
	
}
