package br.ucsal.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_congresso", schema = "public")
public class Congresso {

	@Id
	@GeneratedValue
	private Integer id;
	private String nome;
	private String endereco;
	private String telefone;
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="congresso_id")
	private List <Artigo> artigos;
	@OneToMany(cascade= CascadeType.ALL)
	@JoinColumn(name="congresso_id")
	private List <Participante> participantes;
	
	public List<Artigo> getArtigos() {
		return artigos;
	}
	public void setArtigos(List<Artigo> artigos) {
		this.artigos = artigos;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
