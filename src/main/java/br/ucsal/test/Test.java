package br.ucsal.test;
import java.sql.Connection;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.apache.log4j.Logger;


public class Test {
	private static Connection conexao;
	public static void main(String[] args) throws ClassNotFoundException, SQLException {

	EntityManagerFactory emf = null;
	try {
		emf = Persistence.createEntityManagerFactory("artigos");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
	}catch (Exception e) {
		logger.error("Erro: ", e);
	} finally {
		if (emf != null) {
			emf.close();
		}
	}
}
	
	private static final Logger logger = Logger.getLogger(Test.class);
}