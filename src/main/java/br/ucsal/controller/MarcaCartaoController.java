package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.ucsal.dao.MarcaCartaoDao;
import br.ucsal.model.MarcaCartao;


@Controller
public class MarcaCartaoController {

private MarcaCartaoDao mDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		MarcaCartao m = new MarcaCartao();
		map.put("marcaCartao", m);
		map.put("marcaCartaoList", mDao.getAllMarcaCartao());
		return "marcaCartao";
	}
	
	@RequestMapping(value="/marcaCartao.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute MarcaCartao marcaCartao, BindingResult result, @RequestParam String action, Map<String, Object> map){
		MarcaCartao mResult = new MarcaCartao();
		switch(action.toLowerCase()){	
		case "add":
			mDao.add(marcaCartao);
			mResult = marcaCartao;
			break;
		case "edit":
			mDao.edit(marcaCartao);
			mResult = marcaCartao;
			break;
		case "delete":
			mDao.delete(marcaCartao.getId());
			mResult = new MarcaCartao();
			break;
		case "search":
			MarcaCartao searchedMC = mDao.getMarcaCartao(marcaCartao.getId());
			mResult = searchedMC!=null ? searchedMC : new MarcaCartao();
			break;
		}
		map.put("marcaCartao", mResult);
		map.put("marcaCartaoList", mDao.getAllMarcaCartao());
		return "marcaCartao";
	}
}
