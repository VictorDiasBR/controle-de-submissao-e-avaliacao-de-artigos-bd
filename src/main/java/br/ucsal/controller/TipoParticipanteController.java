package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.ucsal.dao.TipoParticipanteDao;
import br.ucsal.model.TipoParticipante;

@Controller
public class TipoParticipanteController {
	
private TipoParticipanteDao tpDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		TipoParticipante tp = new TipoParticipante();
		map.put("tipoParticipante", tp);
		map.put("tipoParticipanteList", tpDao.getAllTipoParticipante());
		return "tipoParticipante";
	}
	
	@RequestMapping(value="/tipoParticipante.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute TipoParticipante tipoParticipante, BindingResult result, @RequestParam String action, Map<String, Object> map){
		TipoParticipante tpResult = new TipoParticipante();
		switch(action.toLowerCase()){	
		case "add":
			tpDao.add(tipoParticipante);
			tpResult = tipoParticipante;
			break;
		case "edit":
			tpDao.edit(tipoParticipante);
			tpResult = tipoParticipante;
			break;
		case "delete":
			tpDao.delete(tipoParticipante.getId());
			tpResult = new TipoParticipante();
			break;
		case "search":
			TipoParticipante searchedTipoParticipante = tpDao.getTipoParticipante(tipoParticipante.getId());
			tpResult = searchedTipoParticipante!=null ? searchedTipoParticipante : new TipoParticipante();
			break;
		}
		map.put("tipoParticipante", tpResult);
		map.put("tipoParticipanteList", tpDao.getAllTipoParticipante());
		return "tipoParticipante";
	}
}
