package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.ucsal.dao.ParticipanteDao;
import br.ucsal.model.Participante;

@Controller
public class ParticipanteController {

	private ParticipanteDao pDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		Participante p = new Participante();
		map.put("participante", p);
		map.put("participanteList", pDao.getAllParticipante());
		return "participante";
	}
	
	@RequestMapping(value="/participante.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Participante participante, BindingResult result, @RequestParam String action, Map<String, Object> map){
		Participante pResult = new Participante();
		switch(action.toLowerCase()){	
		case "add":
			pDao.add(participante);
			pResult = participante;
			break;
		case "edit":
			pDao.edit(participante);
			pResult = participante;
			break;
		case "delete":
			pDao.delete(participante.getNumInscricao());
			pResult = new Participante();
			break;
		case "search":
			Participante searchedParticipante = pDao.getParticipante(participante.getNumInscricao());
			pResult = searchedParticipante!=null ? searchedParticipante : new Participante();
			break;
		}
		map.put("participante", pResult);
		map.put("participanteList", pDao.getAllParticipante());
		return "participante";
	}
}
