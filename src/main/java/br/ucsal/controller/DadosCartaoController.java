package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.ucsal.dao.DadosCartaoDao;
import br.ucsal.model.DadosCartao;

@Controller
public class DadosCartaoController {
	
private DadosCartaoDao dcDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		DadosCartao m = new DadosCartao();
		map.put("dadosCartao", m);
		map.put("dadosCartaoList", dcDao.getAllDadosCartao());
		return "dadosCartao";
	}
	
	@RequestMapping(value="/dadosCartao.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute DadosCartao dadosCartao, BindingResult result, @RequestParam String action, Map<String, Object> map){
		DadosCartao dResult = new DadosCartao();
		switch(action.toLowerCase()){	
		case "add":
			dcDao.add(dadosCartao);
			dResult = dadosCartao;
			break;
		case "edit":
			dcDao.edit(dadosCartao);
			dResult = dadosCartao;
			break;
		case "delete":
			dcDao.delete(dadosCartao.getId());
			dResult = new DadosCartao();
			break;
		case "search":
			DadosCartao searchedMC = dcDao.getDadosCartao(dadosCartao.getId());
			dResult = searchedMC!=null ? searchedMC : new DadosCartao();
			break;
		}
		map.put("dadosCartao", dResult);
		map.put("dadosCartaoList", dcDao.getAllDadosCartao());
		return "dadosCartao";
	}
}
