package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.ucsal.dao.CongressoDao;
import br.ucsal.model.Congresso;

@Controller
public class CongressoController {

private CongressoDao cDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		Congresso c = new Congresso();
		map.put("congresso", c);
		map.put("congressoList", cDao.getAllCongresso());
		return "congresso";
	}
	
	@RequestMapping(value="/contato.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Congresso congresso, BindingResult result, @RequestParam String action, Map<String, Object> map){
		
		Congresso cResult = new Congresso();
		switch(action.toLowerCase()){	
		case "add":
			cDao.add(congresso);
			cResult = congresso;
			break;
		case "edit":
			cDao.edit(congresso);
			cResult = congresso;
			break;
		case "delete":
			cDao.delete(congresso.getId());
			cResult = new Congresso();
			break;
		case "search":
			Congresso searchedMC = cDao.getCongresso(congresso.getId());
			cResult = searchedMC!=null ? searchedMC : new Congresso();
			break;
		}
		map.put("congresso", cResult);
		map.put("congressoList", cDao.getAllCongresso());
		return "congresso";
	}
}
