package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import br.ucsal.dao.ContatoDao;
import br.ucsal.model.Contato;

@Controller
public class ContatoController {
private ContatoDao cDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		Contato c = new Contato();
		map.put("contato", c);
		map.put("contatoList", cDao.getAllContato());
		return "contato";
	}
	
	@RequestMapping(value="/contato.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Contato contato, BindingResult result, @RequestParam String action, Map<String, Object> map){
		Contato cResult = new Contato();
		switch(action.toLowerCase()){	
		case "add":
			cDao.add(contato);
			cResult = contato;
			break;
		case "edit":
			cDao.edit(contato);
			cResult = contato;
			break;
		case "delete":
			cDao.delete(contato.getId());
			cResult = new Contato();
			break;
		case "search":
			Contato searchedMC = cDao.getContato(contato.getId());
			cResult = searchedMC!=null ? searchedMC : new Contato();
			break;
		}
		map.put("contato", cResult);
		map.put("contatoList", cDao.getAllContato());
		return "contato";
	}
}
