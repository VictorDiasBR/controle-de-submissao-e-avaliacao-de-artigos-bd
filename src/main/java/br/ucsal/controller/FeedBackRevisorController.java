package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.ucsal.dao.FeedBackRevisorDao;
import br.ucsal.model.FeedBackRevisor;


@Controller
public class FeedBackRevisorController {

	private FeedBackRevisorDao fDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		FeedBackRevisor f = new FeedBackRevisor();
		map.put("feedBackRevisor", f);
		map.put("feedBackRevisorList", fDao.getAllFeedBackRevisor());
		return "feedBackRevisor";
	}
	
	@RequestMapping(value="/marcaCartao.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute FeedBackRevisor feedBackRevisor, BindingResult result, @RequestParam String action, Map<String, Object> map){
		FeedBackRevisor fResult = new FeedBackRevisor();
		switch(action.toLowerCase()){	
		case "add":
			fDao.add(feedBackRevisor);
			fResult = feedBackRevisor;
			break;
		case "edit":
			fDao.edit(feedBackRevisor);
			fResult = feedBackRevisor;
			break;
		case "delete":
			fDao.delete(feedBackRevisor.getId());
			fResult = new FeedBackRevisor();
			break;
		case "search":
			FeedBackRevisor searchedF = fDao.getFeedBackRevisor(feedBackRevisor.getId());
			fResult = searchedF!=null ? searchedF : new FeedBackRevisor();
			break;
		}
		map.put("feedBackRevisor", fResult);
		map.put("feedBackRevisorList", fDao.getAllFeedBackRevisor());
		return "feedBackRevisor";
	}
}
