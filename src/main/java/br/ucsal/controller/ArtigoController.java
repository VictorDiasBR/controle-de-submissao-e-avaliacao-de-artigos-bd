package br.ucsal.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.ucsal.dao.ArtigoDao;
import br.ucsal.model.Artigo;
import br.ucsal.model.Congresso;
import br.ucsal.model.Participante;

@Controller
public class ArtigoController {

private ArtigoDao aDao;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		Artigo a = new Artigo();
		map.put("artigo", a);
		map.put("artigoList", aDao.getAllArtigo());
		return "artigo";
	}
	
	@RequestMapping(value="/artigo.do", method=RequestMethod.POST)
	public String doActions(@ModelAttribute Artigo artigo, BindingResult result, @RequestParam String action, Map<String, Object> map){
		Artigo aResult = new Artigo();
		Participante p = new Participante() ;
		Congresso c = new Congresso();
		switch(action.toLowerCase()){	
		case "add":
			aResult.setId(artigo.getId());
			aResult.setEmailAutor(artigo.getEmailAutor());
			aResult.setNomeAutor(artigo.getNomeAutor());
			aResult.setResumoArtigo(artigo.getResumoArtigo());
			aResult.setTituloArtigo(artigo.getTituloArtigo());
			aResult.setLinkArtigo(artigo.getLinkArtigo());
			
			p.setNumInscricao(artigo.getParticipante().getNumInscricao());
			c.setId(artigo.getCongresso().getId());
			
			aResult.setParticipante(p);
			aResult.setCongresso(c);
			
			aDao.add(aResult);
			break;
		case "edit":
			aDao.edit(artigo);
			aResult = artigo;
			break;
		case "delete":
			aDao.delete(artigo.getId());
			aResult = new Artigo();
			break;
		case "search":
			Artigo searchedMC = aDao.getArtigo(artigo.getId());
			aResult = searchedMC!=null ? searchedMC : new Artigo();
			break;
		}
		map.put("artigo", aResult);
		map.put("artigoList", aDao.getAllArtigo());
		return "artigo";
	}
}
