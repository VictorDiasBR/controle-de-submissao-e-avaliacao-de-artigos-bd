package br.ucsal.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

	private static String URL = "jdbc:postgresql://localhost:5432/artigos";
	private static String USER = "postgres";
	private static String PASSWORD = "postgresql";

	public static Connection conectar() throws SQLException, ClassNotFoundException {
		Class.forName("org.postgresql.Driver");

		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
		return connection;
	}

}
