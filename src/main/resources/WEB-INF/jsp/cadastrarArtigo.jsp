<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastro de Artigo</title>
</head>
  <body>
   <form:form action="artigo.do" method="POST" commandName="artigo">
	<table>
		<tr>
			<td>Titulo:</td>
			<td><form:input path="tituloArtigo" /></td>
		</tr>
		<tr>
			<td>Autor:</td>
			<td><form:input path="nomeAutor" /></td>
		</tr>
		<tr>
			<td>Email:<td>
			<td><form:input path="emailAutor" /></td>
		</tr>
		<tr>
			<td>Resumo:</td>
			<td><form:input path="resumoArtigo" /></td>
		</tr>
		<tr>
			<td>Link do artigo:</td>
			<td><form:input path="linkArtigo" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" name="action" value="Add" />
				<input type="submit" name="action" value="Edit" />
				<input type="submit" name="action" value="Delete" />
				<input type="submit" name="action" value="Search" />
			</td>
		</tr>
	</table>
</form:form>
</body>

</html>