<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student Management</title>
</head>
<body>
<h1>Students Data</h1>
<form:form action="participante.do" method="POST" commandName="participante">
	<table>
		<tr>
			<td>Nome</td>
			<td><form:input path="Nome" /></td>
		</tr>
		<tr>
			<td>Endere�o</td>
			<td><form:input path="endereco" /></td>
		</tr>
		<tr>
			<td>Telefone</td>
			<td><form:input path="telefone" /></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><form:input path="Email" /></td>
		</tr>
			<tr>
			<td>Local Emprego</td>
			<td><form:input path="emprego"/></td>
		</tr>
			<tr>
			<td>Tipo participante</td>
			<td>Comum: <form:radiobutton path="tipo" value="1"/><br>
				Avaliador: <form:radiobutton path="tipo" value="2"/>
			</td>
		</tr>
		
		<tr>
			<td>Nome Titular</td>
			<td><form:input path="Nome_Titular" /></td>
		</tr>
		
		<tr>
			<td>N�mero Cart�o</td>
			<td><form:input path="Numero_Cartao" /></td>
		</tr>
		
		<tr>
			<td>Data Vencimento</td>
			<td><form:input path="Data_Vencimento" /></td>
		</tr>
			<tr>
			<td>Bandeira</td>
			<td>form:select path="skills" items="${bandeiras}"/></td>
		</tr>
		
		<tr>
			<td colspan="2">
				<input type="submit" name="action" value="Salvar" />
			</td>
		</tr>
	</table>
</form:form>
<br>
</body>
</html>